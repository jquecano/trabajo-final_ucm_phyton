# Trabajo final_UCM_Phyton

Trabajo final Desarrollo para el pentestin Python.

Especialización en Ciberseguridad - Desarrollo para el pentestig 2022-I

Profesor: Cesar Maraya 

El proyecto consiste en realizar un programa que realize ARP spoofing e intercepte la comunicacion entre dos host para obtener información sensible, como credenciales de acceso para almacenarla en un .json.


## Requisitos

Es neceario conocer la ip de la victima y que el equipo atacante se encuentre en el mismo segmento de red.

Importante validar que los equipos tienen comunicación, realizando ping en ambos equipos.

Para que el programa ejecute es necesario instalar scapy y scapy.http

Comandos:

pip install scapy
pip install scapy_http


## Uso

El proyecto es realizado para practicar y desarrollar los conocimientos adquiridos en la asignatura.

Es un proyecto sencillo que muestra la forma en que se puede realizar un ataque en "Hombre en el medio" en un entorno controlado

Es de uso educativo.


## Escenario


El escenario de la prueba es el siguiente:

En un ambiente vitual se tienen dos maquinas virtuales: Windows 7 que este caso sera la victima y un Kali Linux que realizara el ataque

![1](escenario.png)


## Link del repositorio

https://gitlab.com/jquecano/trabajo-final_ucm_phyton.git



